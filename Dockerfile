FROM node:12.16.1

WORKDIR /app/

ADD package.json .

RUN yarn install --production=true

ADD dist ./dist

CMD [ "yarn", "start" ]

EXPOSE 8081
