import dotenv from 'dotenv';
dotenv.config();

export const USER_NAME_DB = process.env.USER_NAME_DB;
export const PASSWORD_DB = process.env.PASSWORD_DB;
export const DATABASE = process.env.DATABASE;
