type ProviderConfig = {
    [key: string]: ProviderConfigInfo;
  };
  
  export type ProviderConfigInfo = {
    name: string;
    rpcUrl: string;
    chainId: number;
    factory: string;
    pairCodeHash: string;
    router: RouterConfig;
  };
  
  type RouterConfig = {
    abi: any;
    contractAddress: string;
  };
  
  export const SWAP_LOG_ABI = [
    {
      type: 'uint256',
      name: 'amount0In',
    },
    {
      type: 'uint256',
      name: 'amount1In',
    },
    {
      type: 'uint256',
      name: 'amount0Out',
    },
    {
      type: 'uint256',
      name: 'amount1Out',
    },
  ];
  
  export const PAIR_ABI = [
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: 'address',
          name: 'owner',
          type: 'address',
        },
        {
          indexed: true,
          internalType: 'address',
          name: 'spender',
          type: 'address',
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'value',
          type: 'uint256',
        },
      ],
      name: 'Approval',
      type: 'event',
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: 'address',
          name: 'sender',
          type: 'address',
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'amount0',
          type: 'uint256',
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'amount1',
          type: 'uint256',
        },
        {
          indexed: true,
          internalType: 'address',
          name: 'to',
          type: 'address',
        },
      ],
      name: 'Burn',
      type: 'event',
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: 'address',
          name: 'sender',
          type: 'address',
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'amount0',
          type: 'uint256',
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'amount1',
          type: 'uint256',
        },
      ],
      name: 'Mint',
      type: 'event',
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: 'address',
          name: 'sender',
          type: 'address',
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'amount0In',
          type: 'uint256',
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'amount1In',
          type: 'uint256',
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'amount0Out',
          type: 'uint256',
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'amount1Out',
          type: 'uint256',
        },
        {
          indexed: true,
          internalType: 'address',
          name: 'to',
          type: 'address',
        },
      ],
      name: 'Swap',
      type: 'event',
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: 'uint112',
          name: 'reserve0',
          type: 'uint112',
        },
        {
          indexed: false,
          internalType: 'uint112',
          name: 'reserve1',
          type: 'uint112',
        },
      ],
      name: 'Sync',
      type: 'event',
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: 'address',
          name: 'from',
          type: 'address',
        },
        {
          indexed: true,
          internalType: 'address',
          name: 'to',
          type: 'address',
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'value',
          type: 'uint256',
        },
      ],
      name: 'Transfer',
      type: 'event',
    },
    {
      constant: true,
      inputs: [],
      name: 'DOMAIN_SEPARATOR',
      outputs: [
        {
          internalType: 'bytes32',
          name: '',
          type: 'bytes32',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'MINIMUM_LIQUIDITY',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      payable: false,
      stateMutability: 'pure',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'PERMIT_TYPEHASH',
      outputs: [
        {
          internalType: 'bytes32',
          name: '',
          type: 'bytes32',
        },
      ],
      payable: false,
      stateMutability: 'pure',
      type: 'function',
    },
    {
      constant: true,
      inputs: [
        {
          internalType: 'address',
          name: 'owner',
          type: 'address',
        },
        {
          internalType: 'address',
          name: 'spender',
          type: 'address',
        },
      ],
      name: 'allowance',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'spender',
          type: 'address',
        },
        {
          internalType: 'uint256',
          name: 'value',
          type: 'uint256',
        },
      ],
      name: 'approve',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool',
        },
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      constant: true,
      inputs: [
        {
          internalType: 'address',
          name: 'owner',
          type: 'address',
        },
      ],
      name: 'balanceOf',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'to',
          type: 'address',
        },
      ],
      name: 'burn',
      outputs: [
        {
          internalType: 'uint256',
          name: 'amount0',
          type: 'uint256',
        },
        {
          internalType: 'uint256',
          name: 'amount1',
          type: 'uint256',
        },
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'decimals',
      outputs: [
        {
          internalType: 'uint8',
          name: '',
          type: 'uint8',
        },
      ],
      payable: false,
      stateMutability: 'pure',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'factory',
      outputs: [
        {
          internalType: 'address',
          name: '',
          type: 'address',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'getReserves',
      outputs: [
        {
          internalType: 'uint112',
          name: 'reserve0',
          type: 'uint112',
        },
        {
          internalType: 'uint112',
          name: 'reserve1',
          type: 'uint112',
        },
        {
          internalType: 'uint32',
          name: 'blockTimestampLast',
          type: 'uint32',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: '',
          type: 'address',
        },
        {
          internalType: 'address',
          name: '',
          type: 'address',
        },
      ],
      name: 'initialize',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'kLast',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'to',
          type: 'address',
        },
      ],
      name: 'mint',
      outputs: [
        {
          internalType: 'uint256',
          name: 'liquidity',
          type: 'uint256',
        },
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'name',
      outputs: [
        {
          internalType: 'string',
          name: '',
          type: 'string',
        },
      ],
      payable: false,
      stateMutability: 'pure',
      type: 'function',
    },
    {
      constant: true,
      inputs: [
        {
          internalType: 'address',
          name: 'owner',
          type: 'address',
        },
      ],
      name: 'nonces',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'owner',
          type: 'address',
        },
        {
          internalType: 'address',
          name: 'spender',
          type: 'address',
        },
        {
          internalType: 'uint256',
          name: 'value',
          type: 'uint256',
        },
        {
          internalType: 'uint256',
          name: 'deadline',
          type: 'uint256',
        },
        {
          internalType: 'uint8',
          name: 'v',
          type: 'uint8',
        },
        {
          internalType: 'bytes32',
          name: 'r',
          type: 'bytes32',
        },
        {
          internalType: 'bytes32',
          name: 's',
          type: 'bytes32',
        },
      ],
      name: 'permit',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'price0CumulativeLast',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'price1CumulativeLast',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'to',
          type: 'address',
        },
      ],
      name: 'skim',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'uint256',
          name: 'amount0Out',
          type: 'uint256',
        },
        {
          internalType: 'uint256',
          name: 'amount1Out',
          type: 'uint256',
        },
        {
          internalType: 'address',
          name: 'to',
          type: 'address',
        },
        {
          internalType: 'bytes',
          name: 'data',
          type: 'bytes',
        },
      ],
      name: 'swap',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'symbol',
      outputs: [
        {
          internalType: 'string',
          name: '',
          type: 'string',
        },
      ],
      payable: false,
      stateMutability: 'pure',
      type: 'function',
    },
    {
      constant: false,
      inputs: [],
      name: 'sync',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'token0',
      outputs: [
        {
          internalType: 'address',
          name: '',
          type: 'address',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'token1',
      outputs: [
        {
          internalType: 'address',
          name: '',
          type: 'address',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: true,
      inputs: [],
      name: 'totalSupply',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function',
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'to',
          type: 'address',
        },
        {
          internalType: 'uint256',
          name: 'value',
          type: 'uint256',
        },
      ],
      name: 'transfer',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool',
        },
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'from',
          type: 'address',
        },
        {
          internalType: 'address',
          name: 'to',
          type: 'address',
        },
        {
          internalType: 'uint256',
          name: 'value',
          type: 'uint256',
        },
      ],
      name: 'transferFrom',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool',
        },
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function',
    },
  ];
  
  export const PROVIDER_CONFIG: ProviderConfig = {
    pancakeswap: {
      name: 'pancakeswap',
      rpcUrl:
        'https://bsc-dataseed.binance.org/' ||
        'wss://data-seed-prebsc-1-s1.binance.org:8545', //testnet
      chainId: 56,
      factory: '0xcA143Ce32Fe78f1f7019d7d551a6402fC5350c73',
      pairCodeHash:
        '0x00fb7f630766e6a796048ea87d01acd3068e8ff67d078148a3fa3f4a84f69bd5',
      router: {
        abi: [
          {
            inputs: [
              { internalType: 'address', name: '_factory', type: 'address' },
              { internalType: 'address', name: '_WETH', type: 'address' },
            ],
            stateMutability: 'nonpayable',
            type: 'constructor',
          },
          {
            inputs: [],
            name: 'WETH',
            outputs: [{ internalType: 'address', name: '', type: 'address' }],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'tokenA', type: 'address' },
              { internalType: 'address', name: 'tokenB', type: 'address' },
              {
                internalType: 'uint256',
                name: 'amountADesired',
                type: 'uint256',
              },
              {
                internalType: 'uint256',
                name: 'amountBDesired',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
              { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'addLiquidity',
            outputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              {
                internalType: 'uint256',
                name: 'amountTokenDesired',
                type: 'uint256',
              },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'addLiquidityETH',
            outputs: [
              { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
            ],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [],
            name: 'factory',
            outputs: [{ internalType: 'address', name: '', type: 'address' }],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveIn', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveOut', type: 'uint256' },
            ],
            name: 'getAmountIn',
            outputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
            ],
            stateMutability: 'pure',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveIn', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveOut', type: 'uint256' },
            ],
            name: 'getAmountOut',
            outputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
            ],
            stateMutability: 'pure',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
            ],
            name: 'getAmountsIn',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
            ],
            name: 'getAmountsOut',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveA', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveB', type: 'uint256' },
            ],
            name: 'quote',
            outputs: [
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
            ],
            stateMutability: 'pure',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'tokenA', type: 'address' },
              { internalType: 'address', name: 'tokenB', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
              { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'removeLiquidity',
            outputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'removeLiquidityETH',
            outputs: [
              { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'removeLiquidityETHSupportingFeeOnTransferTokens',
            outputs: [
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
              { internalType: 'bool', name: 'approveMax', type: 'bool' },
              { internalType: 'uint8', name: 'v', type: 'uint8' },
              { internalType: 'bytes32', name: 'r', type: 'bytes32' },
              { internalType: 'bytes32', name: 's', type: 'bytes32' },
            ],
            name: 'removeLiquidityETHWithPermit',
            outputs: [
              { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
              { internalType: 'bool', name: 'approveMax', type: 'bool' },
              { internalType: 'uint8', name: 'v', type: 'uint8' },
              { internalType: 'bytes32', name: 'r', type: 'bytes32' },
              { internalType: 'bytes32', name: 's', type: 'bytes32' },
            ],
            name: 'removeLiquidityETHWithPermitSupportingFeeOnTransferTokens',
            outputs: [
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'tokenA', type: 'address' },
              { internalType: 'address', name: 'tokenB', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
              { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
              { internalType: 'bool', name: 'approveMax', type: 'bool' },
              { internalType: 'uint8', name: 'v', type: 'uint8' },
              { internalType: 'bytes32', name: 'r', type: 'bytes32' },
              { internalType: 'bytes32', name: 's', type: 'bytes32' },
            ],
            name: 'removeLiquidityWithPermit',
            outputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapETHForExactTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactETHForTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactETHForTokensSupportingFeeOnTransferTokens',
            outputs: [],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForETH',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForETHSupportingFeeOnTransferTokens',
            outputs: [],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForTokensSupportingFeeOnTransferTokens',
            outputs: [],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'uint256', name: 'amountInMax', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapTokensForExactETH',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'uint256', name: 'amountInMax', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapTokensForExactTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          { stateMutability: 'payable', type: 'receive' },
        ],
        contractAddress:
          '0x10ED43C718714eb63d5aA57B78B54704E256024E' ||
          '0x9Ac64Cc6e4415144C455BD8E4837Fea55603e5c3', // testnet
      },
    },
    mdex: {
      name: 'mdex',
      rpcUrl: 'https://http-mainnet-node.huobichain.com',
      chainId: 128,
      factory: '0xb0b670fc1F7724119963018DB0BfA86aDb22d941',
      pairCodeHash:
        '0x2ad889f82040abccb2649ea6a874796c1601fb67f91a747a80e08860c73ddf24',
      router: {
        abi: [
          {
            inputs: [
              { internalType: 'address', name: '_factory', type: 'address' },
              { internalType: 'address', name: '_WHT', type: 'address' },
            ],
            stateMutability: 'nonpayable',
            type: 'constructor',
          },
          {
            anonymous: false,
            inputs: [
              {
                indexed: true,
                internalType: 'address',
                name: 'previousOwner',
                type: 'address',
              },
              {
                indexed: true,
                internalType: 'address',
                name: 'newOwner',
                type: 'address',
              },
            ],
            name: 'OwnershipTransferred',
            type: 'event',
          },
          {
            inputs: [],
            name: 'WHT',
            outputs: [{ internalType: 'address', name: '', type: 'address' }],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'tokenA', type: 'address' },
              { internalType: 'address', name: 'tokenB', type: 'address' },
              {
                internalType: 'uint256',
                name: 'amountADesired',
                type: 'uint256',
              },
              {
                internalType: 'uint256',
                name: 'amountBDesired',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
              { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'addLiquidity',
            outputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              {
                internalType: 'uint256',
                name: 'amountTokenDesired',
                type: 'uint256',
              },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'addLiquidityETH',
            outputs: [
              { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
            ],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [],
            name: 'factory',
            outputs: [{ internalType: 'address', name: '', type: 'address' }],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveIn', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveOut', type: 'uint256' },
            ],
            name: 'getAmountIn',
            outputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
            ],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveIn', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveOut', type: 'uint256' },
            ],
            name: 'getAmountOut',
            outputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
            ],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
            ],
            name: 'getAmountsIn',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
            ],
            name: 'getAmountsOut',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'account', type: 'address' },
            ],
            name: 'isOwner',
            outputs: [{ internalType: 'bool', name: '', type: 'bool' }],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [],
            name: 'owner',
            outputs: [{ internalType: 'address', name: '', type: 'address' }],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'tokenA', type: 'address' },
              { internalType: 'address', name: 'tokenB', type: 'address' },
            ],
            name: 'pairFor',
            outputs: [{ internalType: 'address', name: 'pair', type: 'address' }],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveA', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveB', type: 'uint256' },
            ],
            name: 'quote',
            outputs: [
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
            ],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'tokenA', type: 'address' },
              { internalType: 'address', name: 'tokenB', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
              { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'removeLiquidity',
            outputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'removeLiquidityETH',
            outputs: [
              { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'removeLiquidityETHSupportingFeeOnTransferTokens',
            outputs: [
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
              { internalType: 'bool', name: 'approveMax', type: 'bool' },
              { internalType: 'uint8', name: 'v', type: 'uint8' },
              { internalType: 'bytes32', name: 'r', type: 'bytes32' },
              { internalType: 'bytes32', name: 's', type: 'bytes32' },
            ],
            name: 'removeLiquidityETHWithPermit',
            outputs: [
              { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
              { internalType: 'bool', name: 'approveMax', type: 'bool' },
              { internalType: 'uint8', name: 'v', type: 'uint8' },
              { internalType: 'bytes32', name: 'r', type: 'bytes32' },
              { internalType: 'bytes32', name: 's', type: 'bytes32' },
            ],
            name: 'removeLiquidityETHWithPermitSupportingFeeOnTransferTokens',
            outputs: [
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'tokenA', type: 'address' },
              { internalType: 'address', name: 'tokenB', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
              { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
              { internalType: 'bool', name: 'approveMax', type: 'bool' },
              { internalType: 'uint8', name: 'v', type: 'uint8' },
              { internalType: 'bytes32', name: 'r', type: 'bytes32' },
              { internalType: 'bytes32', name: 's', type: 'bytes32' },
            ],
            name: 'removeLiquidityWithPermit',
            outputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [],
            name: 'renounceOwnership',
            outputs: [],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: '_swapMininng', type: 'address' },
            ],
            name: 'setSwapMining',
            outputs: [],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapETHForExactTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactETHForTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactETHForTokensSupportingFeeOnTransferTokens',
            outputs: [],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForETH',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForETHSupportingFeeOnTransferTokens',
            outputs: [],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForTokensSupportingFeeOnTransferTokens',
            outputs: [],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [],
            name: 'swapMining',
            outputs: [{ internalType: 'address', name: '', type: 'address' }],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'uint256', name: 'amountInMax', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapTokensForExactETH',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'uint256', name: 'amountInMax', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapTokensForExactTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'newOwner', type: 'address' },
            ],
            name: 'transferOwnership',
            outputs: [],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          { stateMutability: 'payable', type: 'receive' },
        ],
        contractAddress: '0xED7d5F38C79115ca12fe6C0041abb22F0A06C300',
      },
    },
    uniswap: {
      name: 'uniswap',
      rpcUrl: 'https://mainnet.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161',
      chainId: 1,
      factory: '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f',
      pairCodeHash:
        '0x96e8ac4277198ff8b6f785478aa9a39f403cb768dd02cbee326c3e7da348845f',
      router: {
        abi: [
          {
            inputs: [
              { internalType: 'address', name: '_factory', type: 'address' },
              { internalType: 'address', name: '_WETH', type: 'address' },
            ],
            stateMutability: 'nonpayable',
            type: 'constructor',
          },
          {
            inputs: [],
            name: 'WETH',
            outputs: [{ internalType: 'address', name: '', type: 'address' }],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'tokenA', type: 'address' },
              { internalType: 'address', name: 'tokenB', type: 'address' },
              {
                internalType: 'uint256',
                name: 'amountADesired',
                type: 'uint256',
              },
              {
                internalType: 'uint256',
                name: 'amountBDesired',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
              { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'addLiquidity',
            outputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              {
                internalType: 'uint256',
                name: 'amountTokenDesired',
                type: 'uint256',
              },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'addLiquidityETH',
            outputs: [
              { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
            ],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [],
            name: 'factory',
            outputs: [{ internalType: 'address', name: '', type: 'address' }],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveIn', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveOut', type: 'uint256' },
            ],
            name: 'getAmountIn',
            outputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
            ],
            stateMutability: 'pure',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveIn', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveOut', type: 'uint256' },
            ],
            name: 'getAmountOut',
            outputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
            ],
            stateMutability: 'pure',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
            ],
            name: 'getAmountsIn',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
            ],
            name: 'getAmountsOut',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'view',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveA', type: 'uint256' },
              { internalType: 'uint256', name: 'reserveB', type: 'uint256' },
            ],
            name: 'quote',
            outputs: [
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
            ],
            stateMutability: 'pure',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'tokenA', type: 'address' },
              { internalType: 'address', name: 'tokenB', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
              { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'removeLiquidity',
            outputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'removeLiquidityETH',
            outputs: [
              { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'removeLiquidityETHSupportingFeeOnTransferTokens',
            outputs: [
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
              { internalType: 'bool', name: 'approveMax', type: 'bool' },
              { internalType: 'uint8', name: 'v', type: 'uint8' },
              { internalType: 'bytes32', name: 'r', type: 'bytes32' },
              { internalType: 'bytes32', name: 's', type: 'bytes32' },
            ],
            name: 'removeLiquidityETHWithPermit',
            outputs: [
              { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'token', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              {
                internalType: 'uint256',
                name: 'amountTokenMin',
                type: 'uint256',
              },
              { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
              { internalType: 'bool', name: 'approveMax', type: 'bool' },
              { internalType: 'uint8', name: 'v', type: 'uint8' },
              { internalType: 'bytes32', name: 'r', type: 'bytes32' },
              { internalType: 'bytes32', name: 's', type: 'bytes32' },
            ],
            name: 'removeLiquidityETHWithPermitSupportingFeeOnTransferTokens',
            outputs: [
              { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'address', name: 'tokenA', type: 'address' },
              { internalType: 'address', name: 'tokenB', type: 'address' },
              { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
              { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
              { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
              { internalType: 'bool', name: 'approveMax', type: 'bool' },
              { internalType: 'uint8', name: 'v', type: 'uint8' },
              { internalType: 'bytes32', name: 'r', type: 'bytes32' },
              { internalType: 'bytes32', name: 's', type: 'bytes32' },
            ],
            name: 'removeLiquidityWithPermit',
            outputs: [
              { internalType: 'uint256', name: 'amountA', type: 'uint256' },
              { internalType: 'uint256', name: 'amountB', type: 'uint256' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapETHForExactTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactETHForTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactETHForTokensSupportingFeeOnTransferTokens',
            outputs: [],
            stateMutability: 'payable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForETH',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForETHSupportingFeeOnTransferTokens',
            outputs: [],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
              { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapExactTokensForTokensSupportingFeeOnTransferTokens',
            outputs: [],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'uint256', name: 'amountInMax', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapTokensForExactETH',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          {
            inputs: [
              { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
              { internalType: 'uint256', name: 'amountInMax', type: 'uint256' },
              { internalType: 'address[]', name: 'path', type: 'address[]' },
              { internalType: 'address', name: 'to', type: 'address' },
              { internalType: 'uint256', name: 'deadline', type: 'uint256' },
            ],
            name: 'swapTokensForExactTokens',
            outputs: [
              { internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' },
            ],
            stateMutability: 'nonpayable',
            type: 'function',
          },
          { stateMutability: 'payable', type: 'receive' },
        ],
        contractAddress: '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D',
      },
    },
  };
  