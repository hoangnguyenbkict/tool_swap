import cors from "cors";
import express from "express";
import { createServer } from "http";
import { Server, Socket } from "socket.io";
import swaggerUi from "swagger-ui-express";
import JSBI from 'jsbi'
import { pancakeRouter } from "./routers/pancakeRouter";
export { JSBI }

export {
  BigintIsh,
  ChainId,
  TradeType,
  Rounding,
  FACTORY_ADDRESS,
  INIT_CODE_HASH,
  MINIMUM_LIQUIDITY
} from './constants'

export * from './errors'
export * from './entities'
export * from './router'
export * from './fetcher'

const swaggerDocument = require("./swagger.json");
const expressApp = express();
const httpServer = createServer(expressApp);
const io = new Server(httpServer, { cors: { origin: "*" } });


expressApp.use(express.json());
expressApp.use(express.urlencoded({ extended: true }));
expressApp.use(cors());

expressApp.use(`/api/v1`, pancakeRouter);
expressApp.use("/", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

httpServer.listen(process.env.PORT || 3199);
