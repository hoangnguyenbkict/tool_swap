export interface InputSwapExactETHforTokenModel {
    PrivateKey: string,
    AddressTokenOutput: string,
    AmountIn: string,
    SlippageTolerance: string,
    Loop: Number
}