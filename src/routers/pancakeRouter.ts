import { InputSwapExactETHforTokenModel } from './../models/InputSwapModel';
import { tradeService } from './../services/tradeService';
import { Router, NextFunction, Request } from 'express';
const pancakeRouter = Router();

// pancakeRouter.get("/autoTrade", async function (req, res) {
//     const data = await tradeService.autoTrade();
//     res.json(data)
// });

pancakeRouter.post("/SwapExactBNBtoToken", async function (req: Request, res) {
    const params: InputSwapExactETHforTokenModel = req.body;
    const data = await tradeService.SwapExactBNBtoToken(params);
    res.json(data)
});

export { pancakeRouter };
