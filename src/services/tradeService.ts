import { InputSwapExactETHforTokenModel } from './../models/InputSwapModel';
import { CurrencyAmount } from './../entities/fractions/currencyAmount';
import { Fraction } from './../entities/fractions/fraction';
import { TradeOptions } from './../router';
import { Pair } from './../entities/pair';
import { NextFunction } from 'express';
import { TradeType } from './../constants';
import { TokenAmount } from './../entities/fractions/tokenAmount';
import { Token, WETH } from './../entities/token';
import { ChainId } from '../constants';
import { Router } from '../router';
import { ETHER, Percent, Route, Trade } from '../entities';
import ABI from '../abis/ERC20.json'
import { Fetcher } from '../fetcher';
import { PROVIDER_CONFIG } from '../config/provider.config';
const ethers = require('ethers')
const Web3 = require('web3');
import {
    COIN_ERC20_ABI,
    MAX_APPROVE_AMOUNT,
} from '../config/broker.config';

const { JsonRpcProvider } = require("@ethersproject/providers");

// const CAKE = new Token(
//     ChainId.TESTNET,
//     "0xf9f93cf501bfadb6494589cb4b4c15de49e85d0e",
//     18,
//     "CAKE",
//     "PancakeSwap Token"
// );
const WBNB = new Token(
    ChainId.MAINNET,
    "0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c",
    18,
    "WBNB",
    "Wrapped BNB"
);

const USDT = new Token(
    ChainId.MAINNET,
    "0x55d398326f99059ff775485246999027b3197955",
    18,
    "USDT",
    "Tether USD"
);


const CAKE = new Token(
    ChainId.MAINNET,
    "0x0E09FaBB73Bd3Ade0a17ECC321fD13a19e81cE82",
    18,
    "CAKE",
    "PancakeSwap Token"
);

const BUSD = new Token(
    ChainId.MAINNET,
    "0xe9e7cea3dedca5984780bafc599bd69add087d56",
    18,
    "BUSD",
    "Binance-Peg BUSD Token"
);

const slippageTolerance = new Percent("100", "10000"); // 50 bips, or 0.50%

// const amountOutMin = trade.minimumAmountOut(slippageTolerance).raw; // needs to be converted to e.g. hex
// const path = [WETH[CAKE.chainId].address, CAKE.address];
// const to = "0x6cf6Fd827D02871840282bd3E9106EBFF912d55C"; // should be a checksummed recipient address
// const deadline = Math.floor(Date.now() / 1000) + 60 * 20; // 20 minutes from the current Unix time
// const value = trade.inputAmount.raw; // needs to be converted to e.g. hex
class Options implements TradeOptions {
    allowedSlippage: Percent;
    ttl: number;
    recipient: string;
    constructor(allowedSlippage: Percent, ttl: number, recipient: string) {
        this.allowedSlippage = allowedSlippage;
        this.ttl = ttl;
        this.recipient = recipient
    }
}

const web3 = new Web3('https://bsc-dataseed.binance.org/');

const provider = new JsonRpcProvider('https://bsc-dataseed.binance.org/');

// const account = web3.eth.accounts.wallet.add(''); // private key

class TradeService {
    // async autoTrade() {
    //     try {
    //         // const pair = await Fetcher.fetchPairData(BUSD, CAKE, provider);
    //         const amountIn = web3.utils.toWei('300', 'ether');

    //         // const bestTrade = await Trade.bestTradeExactIn([pair], new TokenAmount(BUSD, amountIn), CAKE, { maxHops: 3, maxNumResults: 1 },)[0]
    //         const account = web3.eth.accounts.wallet.add(''); // private key
    //         // const routerContract = await new web3.eth.Contract(
    //         //     PROVIDER_CONFIG.pancakeswap.router.abi,
    //         //     PROVIDER_CONFIG.pancakeswap.router.contractAddress,
    //         //     { from: account.address },
    //         // );

    //         const inputContract = await new web3.eth.Contract(
    //             COIN_ERC20_ABI,
    //             BUSD.address,
    //             { from: account.address },
    //         );
    //         const approve = await inputContract.methods
    //             .approve(PROVIDER_CONFIG.pancakeswap.router.contractAddress, amountIn)
    //             .send({ from: account.address.toString(), gasPrice: 5000000000, gas: 150000 });
    //         console.log(approve)
    //         // const toBigNumber = web3.utils.toBN;
    //         // const routerAllowance = await inputContract.methods
    //         //     .allowance(account.address, PROVIDER_CONFIG.pancakeswap.router.contractAddress)
    //         //     .call();

    //         // const inputBalance = await inputContract.methods.balanceOf(account.address).call();
    //         // console.log(inputBalance)
    //         // const paramsSwap = Router.swapCallParameters(bestTrade, new Options(slippageTolerance, 60 * 200, account.address.toString()))
    //         // console.log(paramsSwap)
    //         // const amountsOut = await routerContract.methods
    //         //     .getAmountsOut('3000000000000000', pathAddress)
    //         //     .call();
    //         // console.log(amountsOut);

    //         // const amountOutMin = Number(amountsOut[1]) * 95 / 100;
    //         // console.log(amountOutMin)
    //         // console.log(web3.utils.toHex(amountOutMin))

    //         // const tx = await routerContract.methods
    //         //     .swapExactTokensForTokens(
    //         //         paramsSwap.args[0],
    //         //         paramsSwap.args[1],
    //         //         paramsSwap.args[2],
    //         //         account.address.toString(),
    //         //         paramsSwap.args[4],

    //         //     )
    //         //     .send({ from: account.address.toString(), gasPrice: 5000000000, gas: 180000 });
    //         // return tx

    //         // const receipt = await tx.wait(); 
    //         // console.log(receipt)
    //         // const tx = await routerContract.methods
    //         //     .swapExactETHForTokens(
    //         //         web3.utils.toHex('1000000000000000000').toString(),
    //         //         web3.utils.toHex(amountOutMin).toString(),
    //         //         pathAddress,
    //         //         wallet.address.toString(),
    //         //         web3.utils.toHex(Math.floor(Date.now() / 1000) + 60 * 2),
    //         //     )
    //         //     .send({ from: wallet.address.toString(), gasPrice: web3.utils.toHex(5000000000), gas: web3.utils.toHex(290000) });


    //     } catch (error) {
    //         return Promise.reject(error)
    //     }
    // }

    async SwapExactBNBtoToken(inputSwapExactETHforTokenModel: InputSwapExactETHforTokenModel) {
        try {
            const account = web3.eth.accounts.wallet.add(inputSwapExactETHforTokenModel.PrivateKey);
            const tokenOut = new Token(ChainId.MAINNET, inputSwapExactETHforTokenModel.AddressTokenOutput, 18)

            const pairIN = await Fetcher.fetchPairData(WBNB, tokenOut, provider);
            const pairOut = await Fetcher.fetchPairData(tokenOut, WBNB, provider);
            const amountIn = web3.utils.toWei(inputSwapExactETHforTokenModel.AmountIn, 'ether');
            const bestTradeIn = await Trade.bestTradeExactIn([pairIN], CurrencyAmount.ether(amountIn), BUSD, { maxHops: 3, maxNumResults: 1 },)[0]

            const slippageTolerance = new Percent(inputSwapExactETHforTokenModel.SlippageTolerance, "10000");
            const paramsswapExactETHForTokens = Router.swapCallParameters(bestTradeIn, new Options(slippageTolerance, 60 * 20, account.address.toString()))

            const bestTradeOut = await Trade.bestTradeExactIn([pairOut], new TokenAmount(tokenOut, paramsswapExactETHForTokens.args[0].toString()), WETH[ChainId.MAINNET], { maxHops: 3, maxNumResults: 1 },)[0]
            const swapExactTokensForETH = Router.swapCallParameters(bestTradeOut, new Options(slippageTolerance, 60 * 20, account.address.toString()))

            const routerContract = await new web3.eth.Contract(
                PROVIDER_CONFIG.pancakeswap.router.abi,
                PROVIDER_CONFIG.pancakeswap.router.contractAddress,
                { from: account.address },
            );
            for (let i = 0; i < inputSwapExactETHforTokenModel.Loop; i++) {
                const tx = await routerContract.methods
                    .swapExactETHForTokens(
                        paramsswapExactETHForTokens.args[0],
                        paramsswapExactETHForTokens.args[1],
                        paramsswapExactETHForTokens.args[2],
                        paramsswapExactETHForTokens.args[3],
                    )
                    .send({ from: account.address.toString(), value: amountIn, gasPrice: 5000000000, gas: 150000 });
                console.log('BNB -> Token', tx.transactionHash)
                const inputContract = await new web3.eth.Contract(
                    COIN_ERC20_ABI,
                    tokenOut.address,
                    { from: account.address },
                );
                const approve = await inputContract.methods
                    .approve(PROVIDER_CONFIG.pancakeswap.router.contractAddress, paramsswapExactETHForTokens.args[0])
                    .send({ from: account.address.toString(), gasPrice: 5000000000, gas: 150000 });
                console.log('approve Token', approve.transactionHash)
                const tx_back = await routerContract.methods
                    .swapExactTokensForETH(
                        swapExactTokensForETH.args[0],
                        swapExactTokensForETH.args[1],
                        swapExactTokensForETH.args[2],
                        swapExactTokensForETH.args[3],
                        swapExactTokensForETH.args[4],
                    )
                    .send({ from: account.address.toString(), gasPrice: 5000000000, gas: 180000 });

                console.log('Token -> BNB', tx_back.transactionHash)

            }
        } catch (error) {
            return Promise.reject(error)
        }
    }
}

export const tradeService = new TradeService()

