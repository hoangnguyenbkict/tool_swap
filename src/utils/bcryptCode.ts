import bcrypt from 'bcrypt';
class BcryptCode {
    private saltRounds: number = 10;
    constructor() {

    }
    hashingPassWord = async (pw: string): Promise<string> => {
        const salts = await bcrypt.genSaltSync(this.saltRounds);
        const result = await bcrypt.hashSync(pw, salts);
        return result
    }
    comparePassWord = async (pwPlanText: string, pwHashed: string): Promise<boolean> => {
        const result = await bcrypt.compareSync(pwPlanText, pwHashed);
        return result;
    }
}
export const bcryptCode = new BcryptCode();