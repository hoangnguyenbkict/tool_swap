const PERMISSION_USER: string[] = ["SignUp","GetUser"];
const PERMISSION_ADMIN = ["GetUsers"].concat(PERMISSION_USER);

export { PERMISSION_USER, PERMISSION_ADMIN }